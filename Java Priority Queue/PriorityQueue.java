public class PriorityQueue {
    private Interval [] heap; // An array that encodes a max-heap data structure.
    private int size;   // The size of allocated buffer for the heap.
    private int numElements;    // The number of elements currently stored. 

    /**
        Constructor: s is the initial size of the heap.
    */
    private int parent(int i) {
        return i/2;
    }
    
    private int right(int i) { 
        return 2*i + 1;
    }
    
    private int left(int i) { 
        return 2*i;
    }
    
    public PriorityQueue(int s) {
        size = s;
        heap = new Interval[size + 1];  // 1 extra element allows us to use 1-based indexing. The max heap stores intervals keyed on their lengths.
        numElements = 1;
    }

    private void siftUp(int i) {
        while(parent(i) > 0 && heap[i].compareTo(heap[parent(i)]) > 0) {
            Interval temp = heap[i];
            heap[i] = heap[parent(i)];
            heap[parent(i)] = temp;
            i = parent(i);
        }
    }
    
    private void siftDown(int i) {
        while(left(i) < numElements && right(i) < numElements && (heap[left(i)].compareTo(heap[i]) > 0 || heap[right(i)].compareTo(heap[i]) > 0)){
            int s;
            if(heap[left(i)].compareTo(heap[right(i)]) > 0) {
                s = left(i);
            }
            else {
                s = right(i);
            }
            Interval temp = heap[i];
            heap[i] = heap[s];
            heap[s] = temp;
            i = s;
        }
    }
    
    
    /**
        Inserts a new Interval k into the heap. Automatically expands the heap if the buffer allocated is full.
    TODO: Please complete this method.
    */
    public void insert(Interval k) {
        if (numElements == size) {
            // Expand the buffer allocated for the heap to another buffer that is twice as big.
            size = size * 2;
            Interval[] n = new Interval[2*size + 1];
            for(int i = 1; i < numElements; i ++) {
                n[i] = heap[i];
            }
            heap = n;
        }
        // Insert without buffer expansion here.
        heap[numElements] = k;
        siftUp(numElements);
        numElements++;
    }

    /**
        Returns the maximum Interval from the heap (usually the one with the largest length. See the compareTo function of Interval for more details on the comparison.
    TODO: Please complete this method.
    */
    public Interval remove_max() {
        if (numElements == 1) return null; // Retuns null if heap is empty.
        // Remove_max code here.
        Interval ret = heap[1];
        heap[1] = heap[numElements-1];
        siftDown(1);
        numElements--;
        return ret; // Replace this statement with returning the max element (root) in the heap.
    }

    /**
        This function prints the contents of the array that encodes a heap.
    */
    public void print() {
        System.out.println("Printing heap:");
        for (int i = 1; i < numElements; ++i)
            System.out.println(heap[i]);
    }
}
