import java.util.Scanner;

public class AreaUnderACurve {

    /**
        The function f(x) = x^2 + x + 1.
    */
    private static double f(double x) {
        return x * x + x + 1; 
    }

    /**
        Returns an approximation for the area under the curve f(x) between x = a and x = b.
    */
    private static double computeArea(double a, double b) {
        double error = 1e-08; // This is the comparison error. See document for description.
        double area = (b-a)*f(b);
        PriorityQueue q = new PriorityQueue(100);
        q.insert(new Interval(a, b));
        double change = 2*error;
        while(change  >= error) {
            Interval old = q.remove_max();
            double mid = (old.getEnd()+old.getStart()) / 2;
            Interval first = new Interval(old.getStart(), mid);
            Interval second = new Interval(mid, old.getEnd()); 
            q.insert(first);
            q.insert(second);
            double oldArea = area;
            area = oldArea - (old.getEnd() - old.getStart())*f(old.getEnd());
            double newInsert = (first.getEnd() - first.getStart())*f(first.getEnd()) + ((second.getEnd() - second.getStart())*f(second.getEnd()));
            area += newInsert;
            change = Math.abs(area - oldArea);
        }        
        return area; // Remove this statement and return the computed area.
    }

    public static void main(String [] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("We have the function f(x) = x^2 + x + 1.");
        System.out.print("Please enter lower value a: ");
        double a = kb.nextDouble();
        System.out.print("Please enter upper value b: ");
        double b = kb.nextDouble();

        double area = computeArea(a, b);
        System.out.println("\nAn approximation for the area under the curve f(x) \nbetween a = " + a + " and b = " + b + " is " + area);
    }
}
