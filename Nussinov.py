import numpy as np

seq1 = "GCGGAUUUAGCUCAGUUGGGAGAGCGCCAGACUGAAGAUCUGGAGGUCCUGUGUUCGAUCCACAGAAUUCGCACCA"
seq2 = "GGGCGGCUAGCUCAGCGGAAGAGCGCUCGCCUCACACGCGAGAGGUCGUAGGUUCAAGUCCUACGCCGCCCACCA"
seq3 = "GGAGAGGUGCCCGAGUGGCUGAAGGGACACGACUGGAAAUCGUGUAGGGGGGCUUAAACCUCCCUCGCGGGUUCGAAUCCCGCCCUCUCCGCCA"
seq4 = "UGGGGUAUCGCCAAGCGGUAAGGCACCGGAUUCUGAUUCCGGCAAGCGAGGUUCGAAUCCUCGUACCCCAGCCA"
seq5 = "GGGGUAUCGCCAAGCGGUAAGGCACCGGAUUCUGAUUCCGGCAGCGAGGUUCGAAUCCUCGUACCCCAGCCA"
testSeq = "GCGGAAGCUC"

seqArr = [seq1, seq2, seq3, seq4, seq5, testSeq]

def secondary_structure_with_loop_len(s, loop):
    l = len(s)
    DP = [[0 for i in range(l + 1)] for j in range(l + 1)]
    k_arr = [[[] for i in range(l)] for j in range(l)]
    BT = [['s' for i in range(l + 1)] for j in range(l + 1)]
    # Matrix adding array of k values for each i,j in Nussinov matrix
    end = l - 1
    count = 1

    for c in range(1, l):
        for i in range(end):
            j = c+i
            for k in range(loop + i + 1, j + 1):
                if (pairing(s[i], s[k])):
                    k_arr[i][j].append(k)
        end -= 1
    # Array passing through Nussinov matrix in correct order
    end = l - 1
    for c in range(1, l):
        for i in range(end):
            j = c + i
            big = 0
            usedK = 0
            for t in range(len(k_arr[i][j])):
                k = k_arr[i][j][t]
                temp = 1 + DP[i + 1][k - 1] + DP[k + 1][j]
                if temp > big:
                    big = temp
                    usedK = k
            DPcur = DP[i + 1][j]
            if DPcur < big:
                DP[i][j] = big
                BT[i][j] = usedK
            else:
                DP[i][j] = DPcur
                BT[i][j] = 'd'
        end -= 1
    output = ['.' for i in range(l)]
    back_track(BT,0, l-1, output)
    s = (''.join(output))
    return s


def back_track(BT,i,j,output):
    if BT[i][j] == 's':
        return
    if BT[i][j] == 'd':
        back_track(BT,i+1,j,output)
    else:
        k = int(BT[i][j])
        output[i] = '('
        output[k] = ')'
        back_track(BT, i+1,k-1, output)
        back_track(BT, k+1, j, output)

'''
def secondary_structure(s):
    l = len(s)
    DP = [[0 for i in range(l + 1)] for j in range(l + 1)]
    k_arr = [[[] for i in range(l)] for j in range(l)]
    BT = [['s' for i in range(l + 1)] for j in range(l + 1)]
    # Matrix adding array of k values for each i,j in Nussinov matrix
    for i in range(l):
        j = 1 + i
        while j < l:
            for k in range(i, j + 1):
                if (pairing(seq[i], seq[k])):
                    k_arr[i][j].append(k)
            j += 1
    end = l - 1
    # Array passing through Nussinov matrix in correct order
    for c in range(1, l):
        for i in range(end):
            j = c + i
            big = 0
            usedK = 0
            for t in range(len(k_arr[i][j])):
                k = k_arr[i][j][t]
                temp = 1 + DP[i + 1][k - 1] + DP[k + 1][j]
                if temp > big:
                    big = temp
                    usedK = k
            DPcur = DP[i + 1][j]
            if DPcur < big:
                DP[i][j] = big
                BT[i][j] = usedK
            else:
                DP[i][j] = DPcur
                BT[i][j] = 'd'
        end -= 1
    output = ['.' for i in range(l)]
    back_track(BT, 0, l-1, output)
    print(''.join(output))
'''


def max_basepairs(s):
    l = len(s)
    DP = [[0 for i in range(l + 1)] for j in range(l + 1)]
    k_arr = [[[] for i in range(l)] for j in range(l)]
    # Matrix adding array of k values for each i,j in Nussinov matrix
    for i in range(l):
        j = 1 + i
        while j < l:
            for k in range(i, j + 1):
                if (pairing(s[i], s[k])):
                    k_arr[i][j].append(k)
            j += 1
    end = l - 1
    # Array passing through Nussinov matrix in correct order
    for c in range(1, l):
        for i in range(end):
            j = c + i
            big = 0
            for t in range(len(k_arr[i][j])):
                k = k_arr[i][j][t]
                temp = 1 + DP[i + 1][k - 1] + DP[k + 1][j]
                if temp > big: big = temp
            DP[i][j] = max(DP[i + 1][j], big)
        end -= 1
    print(np.matrix(DP))

def pairing(c1, c2):
    if c1 == "A":
        if c2 == "U":
            return True
        else:
            return False
    if c1 == "U":
        if c2 == "A":
            return True
        if c2 == "G":
            return True
        else:
            return False
    if c1 == "G":
        if c2 == "U":
            return True
        if c2 == "C":
            return True
        else:
            return False
    if c1 == "C":
        if c2 == "G":
            return True
        else:
            return False

RNAF1 = "((((((((.((((........)))).(((((.......)))))......((((.......))))))))))))...."
RNAF2 = "(((((((..((((.......)))).(((((.......))))).....(((((.......))))))))))))...."
RNAF3 = "(((((((..(((...........)))((((((.......))))))(((((((......))))))).(((((.......))))))))))))...."
RNAF4 = "(((((((((((...)))))......(((((.......)))))...((((((.......))))))))))))...."
RNAF5 = "((((((((((...)))))......(((((.......)))))..((((((.......)))))))))))....."
RNAFTest = "((....)).."


RNArr = [RNAF1, RNAF2, RNAF3, RNAF4, RNAF5, RNAFTest]

#Method to compare my Secondary Structure to RNAFold's Secondary Structure
def compare():
    for i in range(6): #Rotating though different secondary structures and sequences
        retSS = ""
        retL = -1
        leastDiff = 10000
        curRNAFSS = RNArr[i]
        curSEQ = seqArr[i]
        for j in range(51): #Testing for loop lengths 0 though 50
            curDif = 0
            curSS = secondary_structure_with_loop_len(curSEQ, j)
            for k in range(len(curSEQ)): #comparing character by character
                if curSS[k] != curRNAFSS[k]:
                    curDif = curDif + 1
            if(curDif < leastDiff):
                retL = j
                leastDiff = curDif
                retSS = curSS
        print("Sequence analyzed:", curSEQ)
        print("My best minimum loop length:", retL, "\nNumber of differences from RNAFold:", leastDiff)
        print("My Nussinov output:\n", retSS)
        print("RNAFold's output:\n", RNArr[i])
        print("\n")

compare()

