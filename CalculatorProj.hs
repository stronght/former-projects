module Main where
import Data.Char
import Data.List

type Vars = String

data AExpr = Var Vars | Const Float
           | Add AExpr AExpr | Mul AExpr AExpr | Sub AExpr AExpr 
           | Div AExpr AExpr | Root AExpr
           | Pow AExpr AExpr 
     deriving (Eq,Show) 


data BOps = AddOp | MulOp | SubOp | DivOp | RootOp | PowOp deriving (Show,Eq)

data Token = VSym Vars | BOp BOps | CSym Float
           | LPar | RPar
           | PA AExpr
           | Err String
  deriving (Show,Eq)


-- Values and Environments
type Value = Float
type Env = [(Vars,Value)]
type FunEnv = [(Vars, AExpr)]


eval :: Env -> AExpr -> Float
eval e (Var x) = case lookup x e of
                  Just v -> v
                  Nothing -> error $ "No value for variable " ++ x
eval e (Const n) = n
eval e (Add e1 e2) = eval e e1 + eval e e2
eval e (Sub e1 e2) = eval e e1 - eval e e2
eval e (Mul e1 e2) = eval e e1 * eval e e2
eval e (Div e1 e2) = eval e e1 / eval e e2
eval e (Root e1) = sqrt (eval e e1)
eval e (Pow e1 e2) = eval e e1 ** eval e e2

-- The Lexer

-- A constant must begin with a digit, be potenially followed by a decimal point, then followed by one or more digits.
isCSym :: String -> Bool
isCSym = let q0 "" = False
             q0 (x:xs) = isDigit x && q1 xs
             q1 "" = True
             q1 (x:xs) = if (isDigit x) then q1 xs else (if (x == '.') then q2 xs else False)
             q2 "" = False
             q2 (x:xs) = isDigit x && q3 xs
             q3 "" = True
             q3 (x:xs) = isDigit x && q3 xs
          in q0

-- A variable must begin with a lowercase letter,
-- and be followed by zero or more letters, numbers, _, or '
isVSym :: String -> Bool
isVSym = let q0 ""     = False
             q0 (x:xs) | x `elem` ['a'..'z'] = q1 xs
                       | otherwise = False
             q1 [] = True
             q1 (x:xs) = (isLetter x || isDigit x || elem x "_'") && q1 xs
          in q0

preproc :: String -> String
preproc "" = ""
preproc ('(':xs) = ' ' : '(' : ' ' : preproc xs
preproc (')':xs) = ' ' : ')' : ' ' : preproc xs
preproc ('*':xs) = ' ' : '*' : ' ' : preproc xs
preproc ('+':xs) = ' ' : '+' : ' ' : preproc xs
preproc ('-':xs) = ' ' : '-' : ' ' : preproc xs
preproc('/':xs) = ' ' : '/' : ' ' : preproc xs
preproc('s':'q':'r':'t':xs) = ' ' : 's':'q':'r':'t': ' ' : preproc xs
preproc('^':xs) = ' ' : '^' : ' ' : preproc xs
preproc (x:xs) = x : preproc xs


analyze :: [String] -> [Token]
analyze [] = []
analyze ("(":xs) = LPar : analyze xs
analyze (")":xs) = RPar : analyze xs
analyze ("*":xs) = BOp MulOp : analyze xs
analyze ("+":xs) = BOp AddOp : analyze xs
analyze ("-":xs) = BOp SubOp : analyze xs
analyze ("/":xs) = BOp DivOp : analyze xs
analyze ("sqrt":xs) = BOp RootOp : analyze xs
analyze ("^":xs) = BOp PowOp : analyze xs
analyze (x:xs) | isCSym x = CSym (read x) : analyze xs
analyze (x:xs) | isVSym x = VSym x : analyze xs
analyze s = error $ "Token error: " ++ concat s

lexer :: String -> [Token]
lexer = analyze . words . preproc

update :: (Vars,Value) -> Env -> Env
update (x,v) ((y,w):es) | x == y    = (x,v) : es
                        | otherwise = (y,w) : update (x,v) es
update (x,v) [] = [(x,v)]

updateFun :: (Vars, AExpr) -> FunEnv -> FunEnv
updateFun (x,v) ((y,w):es) | x == y    = (x,v) : es
                           | otherwise = (y,w) : updateFun (x,v) es
updateFun (x,v) [] = [(x,v)]

-- The Parser
parser :: [Token] -> AExpr
parser input = sr input []

sr :: [Token] -> [Token] -> AExpr
sr (Err s : input) _ = error ("Lexical error: " ++ s)
sr [] [PA e] = e
sr input (VSym v : rs) = sr input (PA (Var v) : rs)   
sr input (CSym n : rs) = sr input (PA (Const n) : rs) 
sr input (PA e2 : BOp AddOp : PA e1 : rs) = sr input (PA (Add e1 e2) : rs) 
sr input (PA e2 : BOp SubOp : PA e1 : rs) = sr input (PA (Sub e1 e2) : rs) 
sr input (PA e : BOp SubOp : rs) = sr input (PA (Sub (Const 0) e) : rs)
sr input (PA e2 : BOp MulOp : PA e1 : rs) = sr input (PA (Mul e1 e2) : rs) 
sr input (PA e2 : BOp DivOp : PA e1 : rs) = sr input (PA (Div e1 e2) : rs) 
sr input (PA e2 : BOp PowOp : PA e1 : rs) = sr input (PA (Pow e1 e2) : rs) 
sr input (PA e1 : BOp RootOp : rs) = sr input (PA (Root e1) : rs) 
sr input (RPar : PA e : LPar : rs)        = sr input (PA e : rs)
sr (i:input) stack = sr input (i:stack)
sr [] stack = error (show stack)

parseString :: String -> AExpr
parseString = parser . lexer

-- The Main Method
-- An operator is required between characters
-- i.e. 3x+2 should be written as 3*x+2

main :: IO ()
main = do
  putStrLn "Enter an expression you want evaluated: "
  inputA <- getLine
  do
      let expr = parser (lexer inputA)
      let loop a = do
          putStrLn "Enter next command:"
          input <- getLine
          case words input of
            ("new":[]) ->  do 
              putStrLn "Enter a new expression:"
              newA <- getLine
              if newA == "quit" then return () else loop (updateFun ("_", parseString newA) (fst a), (snd a))        
            ("clearFuns":[])    -> do
              putStrLn "All Functions Cleared!"
              putStrLn "Enter a new expression or type quit to quit"
              newA <- getLine
              if newA == "quit" then return () else loop ([("_", (parser (lexer newA)))], snd a)
            ("clearVars":[]) -> putStrLn ("Current variables cleared!") >> loop (fst a, [])
            ("store" : []) -> do
                putStrLn("Enter the function you would like to store:")
                str <- getLine
                putStrLn ("Enter the variable you would like to store this function as")
                var <- getLine
                loop (updateFun (var, parseString str) (fst a), (snd a))
            ("storeCur" : []) -> do
                putStrLn ("Enter the variable you would like to store the current function as")
                var <- getLine
                loop (updateFun (var, snd (head (fst a))) (fst a), (snd a))
            ("eval" : [])   -> putStrLn ("The value of the expression is: "
                                    ++ show (eval (snd a) (snd (head (fst a)))))
                             >> loop a
            ("eval" : var : []) -> case lookup (var) (fst a) of 
                                            Just x -> putStrLn ("The value of the expression is: "
                                                ++ show (eval (snd a) x)) >> loop a
                                            Nothing -> putStrLn("Function not found") >> loop a
            (var : ":=" : val : []) ->
                loop ((fst a), update (var,read val) (snd a))
            ("quit":[])   -> return ()
            (c:[])       -> putStrLn ("Unrecognized command: " ++  c) >>
                        putStrLn ("  type \"quit\" to quit")    >> loop a
      loop ([("_", expr)], []) --(funEnv, Env)
