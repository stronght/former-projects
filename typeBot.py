#Bot code starts at ~ line 163
import discord
import os
import re

listTypes = ["grass", "fire", "water", "normal", "electric", "psychic", "fighting", "rock", "ground", "flying", "bug", "poison", "dark", "ghost", "ice", "steel", "dragon", "fairy"]

#Creating tuples of types and integers
pkmnType = {"grass":1, "fire":1, "water":1, "normal":1, "electric":1, "psychic":1, "fighting":1, "rock":1, "ground":1, "flying":1, "bug":1, "poison":1, "dark":1, "ghost":1, "ice":1, "steel":1, "dragon":1, "fairy":1}

#Helper functions
def uAll(dic, t1, t2):
  updateDic(dic, t1, .5,)
  updateDic(dic, t2, 2)
  
def uAll0(dic, t1, t2, t3):
  updateDic(dic, t1, .5,)
  updateDic(dic, t2, 2)
  updateDic(dic, t3, 0)

def updateDic(dic, types, value):
  for i in range(len(types)):
    s = types[i]
    dic[s] = value

def get(dic, key):
  return dic[key]


#Lists for All Pokemon Types
#List looks at the type defensively

#grass
graT = pkmnType.copy()
graR = ["electric", "water", "ground", "grass"]
graW = ["bug","fire","flying","ice","poison"]
updateDic(graT, graR, .5)
updateDic(graT, graW, 2)

#fire
firT = pkmnType.copy()
updateDic(firT, ["grass"], .5)
updateDic(firT, ["fire"], .5)
updateDic(firT, ["water"], 2)
updateDic(firT, ["rock"], 2)
updateDic(firT, ["ground"], 2)
updateDic(firT, ["bug"], .5)
updateDic(firT, ["ice"], .5)
updateDic(firT, ["steel"], .5)
updateDic(firT, ["fairy"], .5)

#water
watT = pkmnType.copy()
watR = ["fire", "ice", "steel", "water"]
watW = ["electric","grass"]
updateDic(watT, watR, .5)
updateDic(watT, watW, 2)

#normal
norT = pkmnType.copy()
norW = ["fighting"]
updateDic(norT, ["ghost"], 0)
updateDic(norT, norW, 2)

#electric
eleT = pkmnType.copy()
eleR = ["electric", "flying","steel"]
eleW = ["ground"]
updateDic(eleT, eleR, .5)
updateDic(eleT, eleW, 2)

#psychic
psyT = pkmnType.copy()
psyR = ["fighting","psychic"]
psyW = ["bug","dark","ghost"]
updateDic(psyT, psyR, .5)
updateDic(psyT, psyW, 2)

#fighting
figT = pkmnType.copy()
figR = ["bug","dark","rock"]
figW = ["fairy","flying","psychic"]
updateDic(figT, figR, .5)
updateDic(figT, figW, 2)

#rock
rocT = pkmnType.copy()
rocR = ["fire","flying","normal","poison"]
rocW = ["fighting","grass","ground","water"]
uAll(rocT, rocR, rocW)

#ground
groT = pkmnType.copy()
groR = ["poison","rock"]
groW = ["grass","ice","water"]
groI = ["electric"]
uAll0(groT,groR,groW,groI)

#flying
flyT = pkmnType.copy()
flyR = ["bug","fighting"]
flyW = ["electric","ice","rock"]
flyI = ["ground"]
uAll0(flyT, flyR, flyW, flyI)

#bug
bugT = pkmnType.copy()
bugR = ["fighting","grass","ground"]
bugW = ["fire","flying","rock"]
uAll(bugT, bugR, bugW)

#poison
poiT = pkmnType.copy()
poiR = ["fighting","poison","bug","grass","fairy"]
poiW = ["ground","psychic"]
uAll(poiT,poiR,poiW)

#dark
darT = pkmnType.copy()
darR = ["dark","ghost"]
darW = ["bug","fairy","fighting"]
darI = ["psychic"]
uAll0(darT,darR,darW,darI)

#ghost
ghoT = pkmnType.copy()
ghoR = ["bug","poison"]
ghoW = ["dark","ghost"]
ghoI = ["normal","fighting"]
uAll0(ghoT,ghoR,ghoW,ghoI)

#ice
iceT = pkmnType.copy()
iceR = ["ice"]
iceW = ["fighting","fire","rock","steel"]
uAll(iceT,iceR,iceW)

#steel
steT = pkmnType.copy()
steR = ["bug","dragon","fairy","flying","grass","ice","normal","psychic","rock","steel"]
steW = ["fighting","fire","ground"]
steI = ["poison"]
uAll0(steT,steR,steW,steI)

#dragon
draT = pkmnType.copy()
draR = ["electric","fire","water","grass"]
draW = ["dragon","fairy","ice"]
uAll(draT,draR,draW)

#fairy
faiT = pkmnType.copy()
faiR = ["bug","dark","fighting"]
faiW = ["poison","steel"]
faiI = ["dragon"]
uAll0(faiT,faiR,faiW,faiI)

#Tuples of type and dictonary
listDics = [graT, firT, watT, norT, eleT, psyT, figT, rocT, groT, flyT, bugT, poiT, darT, ghoT, iceT, steT, draT, faiT]

pkmnTypeDic = {listTypes[i]: listDics[i] for i in range(len(listTypes))}

#Actual Bot
client = discord.Client()

@client.event
async def on_ready():
    print("{0.user} is running".format(client))

@client.event

async def on_message(message):
    if message.author == client.user:
        return
    msg = message.content

    if msg.startswith("!type"):
        relevantDics = []
        userList = []
        userStr = msg.split("!type ", 1)[1]
        userList = re.split("\s+", userStr)
        for i in range(len(userList)):
          word = userList[i]
          word = word.lower()
          if word in listTypes:
            relevantDics.append(pkmnTypeDic[word].copy())
        dicLen = len(relevantDics)
        while(dicLen > 1):
          next = relevantDics[dicLen-1]
          pure = relevantDics[0]
          for key in next:
            pure[key] = pure[key]*next[key]
          dicLen = dicLen - 1
        await message.channel.send(relevantDics[0])

client.run(os.getenv("TOKEN"))

