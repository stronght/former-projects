import java.util.Scanner;
public class NQueens{
    static int[][] board;
    static int[][] queens;
    static int count = 0;
    static int n;
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number of queens: ");
        n = scan.nextInt();
        board = new int[n][n];
        queens = new int[n][n];
        check_row(0);
        System.out.println("The number of valid arrangements is " + count);
    }

public static void check_row(int r) {
    int filled = 0;
    for(int i = 0; i < n; i++) {
        if(r != n) {
            filled += board[r][i];
        }
    } 
    if(filled == -n) {
        return;
    }

    if(r == n) {
        count++;
        return;
    }

    for(int c = 0; c < n; c++) {
        if(board[r][c] != -1) {
            addQueen(r, c);
            check_row(r+1);
            removeQueen(r, c);
        }
    }
}

public static void addQueen(int a, int b) {
    for(int i = a; i < n; i++) {
        for(int j = 0; j < n; j++) {
            if(i == a || j == b || j+i == a+b) {
                board[i][j] = -1;
            }
        }
    }
    for(int x = 0; x < n; x++) {
        if(a+x < n && b+x < n) {
            board[a+x][b+x] = -1;
        }
    }
    queens[a][b] = 1;
}

public static void removeQueen(int a, int b) {
    for(int i = a; i < n; i++) {
        for(int j = 0;j < n; j++) {
            if(i == a || j == b || j+i == a+b) {
                board[i][j] = 0;
            }
        }
    }
    for(int x = 0; x < n; x++) {
        if(a+x < n && b+x < n) {
            board[a+x][b+x] = 0;
        }
    }
    queens[a][b] = 0;
    if(a > 0) {
        for(int i = 0; i < a; i++) {
            for(int c = 0; c < n; c++) {
                if(queens[i][c] == 1) {
                    addQueen(i, c);
                }
            }
        }
    }
}

public static void printBoard() {
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            System.out.print(queens[i][j] + " ");
        }
        System.out.println();
    }

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            System.out.print(board[i][j] + " ");
        }
        System.out.println();
    }




}
}
